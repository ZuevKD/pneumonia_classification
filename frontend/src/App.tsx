import React from 'react';
import 'antd/dist/antd.css';
import configureStore from "./store/reducers/configureStore";
import {Provider} from "react-redux";
import {initialState} from "./store/initialState";
import Router from "./Router";

function App() {
    const store = configureStore(initialState)

    return (
        <Provider store={store}>
            <Router/>
        </Provider>
    );
}

export default App;
