import React, {useEffect} from 'react';
import 'antd/dist/antd.css';
import {Table as TableAntd} from 'antd';
import {IPatient} from "../domains";
import {connect} from "react-redux";
import {bindActionCreators, Dispatch} from "redux";
import {loadPatient, loadPatients} from "../../store/actionCreators";
import {useHistory} from "react-router-dom";

interface IStateProps {
    patients: IPatient[];
}

interface IDispatchProps {
    loadPatients: () => void;
    loadPatient: (id: number) => void;
}

type IProps = IDispatchProps & IStateProps;

const Table = ({loadPatients, loadPatient, patients}: IProps) => {
    useEffect(() => {
        loadPatients();
    }, []);

    const history = useHistory();

    const columns = [
        {title: 'Фамилия', dataIndex: 'last_name', key: 'last_name'},
        {title: 'Имя', dataIndex: 'name', key: 'name'},
        {title: 'Отчество', dataIndex: 'middle_name', key: 'middle_name'},
        {title: 'Дата рождения', dataIndex: 'birthday', key: 'birthday'},
    ];


    const patientsData: IPatient[] = patients.map((patient: IPatient) => {
        console.log(patient)
        return {
            ...patient,
            key: patient.id,
        }
    })

    return (
        <TableAntd
            columns={columns}
            dataSource={patientsData}
            onRow={(patient: IPatient) => {
                return {
                    onDoubleClick: () => {
                        history.push("patient", {selectedPatient: patient.id});
                        loadPatient(patient.id);
                    },
                };
            }}
        />
    )
}

const mapStateToProps = (state: any): IStateProps => {
    return ({
        patients: state.mainPage.patients,
    });
};

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators({
        loadPatients,
        loadPatient,
    }, dispatch);

export default connect<IStateProps, IDispatchProps>(
    mapStateToProps,
    mapDispatchToProps,
)(Table);