import React, {useState} from 'react';
import 'antd/dist/antd.css';
import {DatePicker, Input} from 'antd';
import {connect} from "react-redux";
import {bindActionCreators, Dispatch} from "redux";
import {loadPatients} from "../../store/actionCreators";
import {IPatientFilter} from "../domains";
import {DateTime} from "luxon";
import locale from 'antd/es/date-picker/locale/ru_RU';

const {Search} = Input;

interface IDispatchProps {
    loadPatients: (filter: IPatientFilter) => void;
}

type IProps = IDispatchProps;

const SearchField = ({loadPatients}: IProps) => {
    const [filter, setFilter] = useState({} as IPatientFilter);

    const onChangeSearch = (e) => {
        const {value} = e.target;
        let fio = value.split(" ");

        setFilter({
            ...filter,
            last_name: fio[0],
            name: fio[1],
            middle_name: fio[2],
        })
    }

    const onSearch = (value: string) => {
        loadPatients(filter);
    }

    const onChange = (date, dateString) => {
        const birthday = DateTime.fromISO(dateString).setLocale('ru').toISO()
        setFilter({
            ...filter,
            birthday,
        })

        loadPatients({...filter,
            birthday});
    }

    return <Input.Group compact style={{paddingBottom: 16}}>
        <Search
            placeholder="Фамилия Имя Отчество"
            style={{width: '70%', paddingRight: 16}}
            onSearch={onSearch}
            onChange={onChangeSearch}
            enterButton
            allowClear
        />

        <DatePicker
            locale={locale}
            placeholder={'Дата рождения'}
            style={{width: '30%'}}
            allowClear
            onChange={onChange}
        />
    </Input.Group>
}

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators({
        loadPatients,
    }, dispatch);

export default connect<null, IDispatchProps>(
    null,
    mapDispatchToProps,
)(SearchField);