import React from 'react';
import Table from "./Table";
import 'antd/dist/antd.css';
import {Breadcrumb} from 'antd';
import SearchField from "./SearchField";
import LogoutButton from "../LogoutButton";

const MainPage = () => {
    return (
        <>
            <Breadcrumb style={{padding: 16}}>
                <Breadcrumb.Item>
                    <a>Главная</a>
                </Breadcrumb.Item>
            </Breadcrumb>

            <LogoutButton/>

            <div style={{width: "80%", margin: "0 auto"}}>
                <SearchField/>

                <Table/>
            </div>
        </>
    )
};

export default MainPage;