import {Button, Form, Input} from 'antd';
import React from 'react';
import {bindActionCreators, Dispatch} from "redux";
import {auth} from "../../store/actionCreators";
import {connect} from "react-redux";

interface IDispatchProps {
    auth: (username: string, password: string) => void;
}

type IProps = IDispatchProps;

const LoginForm = ({auth}: IProps) => {
    const onFinish = async (values: any) => {
        const {username, password} = values;
        await auth(username, password);
    };

    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <Form
            name="basic"
            initialValues={{remember: true}}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            style={{width: 300, margin: "0 auto", marginTop: 300}}
        >
            <Form.Item
                label="Логин"
                name="username"
                rules={[{required: true, message: 'Пожалуйста, введите Ваш логин!'}]}
            >
                <Input/>
            </Form.Item>

            <Form.Item
                label="Пароль"
                name="password"
                rules={[{required: true, message: 'Пожалуйста, введите Ваш пароль!'}]}
            >
                <Input.Password/>
            </Form.Item>

            <Form.Item>
                <Button type="primary" htmlType="submit">
                    Войти
                </Button>
            </Form.Item>
        </Form>
    );
};

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators({
        auth,
    }, dispatch);

export default connect<null, IDispatchProps>(
    null,
    mapDispatchToProps,
)(LoginForm);
