import {Upload, message} from 'antd';
import {InboxOutlined} from '@ant-design/icons';
import React from 'react';
import {csrftoken} from "./utils";

const {Dragger} = Upload;

interface IProps {
    patientId: number;
    afterUpLoadImage: () => void;
}

export const ImageUploader = ({patientId, afterUpLoadImage}: IProps) => {
    const props = {
        name: 'image',
        action: 'upload-image',
        headers: {
            'X-CSRFToken': csrftoken,
            'Authorization': 'Token ' + localStorage.getItem('token'),
        },
        data: {
            patientId,
        },
        onChange(info) {
            const {status} = info.file;
            if (status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (status === 'done') {
                message.success(`${info.file.name} файл успешно загружен.`);
                afterUpLoadImage();
            } else if (status === 'error') {
                if (info?.file?.response?.detail) {
                    message.error(`${info.file.response.detail}`);
                } else {
                    message.error(`${info.file.name} ошибка при загрузке файла.`);
                }
            }
        },
        beforeUpload: file => {
            if (file.type !== 'image/jpeg') {
                console.log(file.type)
                message.error(`Загрузите снимок с форматом jpeg. Загружаемый файл ${file.name}`);
            }
            return file.type === 'image/jpeg';
        },
    };

    return (
        <Dragger {...props}>
            <p className="ant-upload-drag-icon">
                <InboxOutlined/>
            </p>
            <p className="ant-upload-text">Кликните или перетащите снимок для загрузки</p>
            <p className="ant-upload-hint">
                Поддерживается только формат .jpg
            </p>
        </Dragger>
    );
}
