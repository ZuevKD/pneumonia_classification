import React, {CSSProperties} from 'react';
import 'antd/dist/antd.css';
import {Button, Image, List} from "antd";
import {IImage} from "../domains";
import {CloseOutlined} from '@ant-design/icons';
import {ImageInfo} from "./ImageInfo";
import axios from "axios";

interface IProps {
    urls: IImage[];
    getImagesByPatient: () => void;
}

const image_with_cross: CSSProperties = {
    display: "flex",
    justifyContent: "space-between",
}

export const ListImages = ({urls, getImagesByPatient}: IProps) => {
    const onRemove = (id) => () => {
        axios.delete(`images/${id}`)
            .then(() => getImagesByPatient())
    }

    return (
        <List
            itemLayout="vertical"
            size="small"
            dataSource={urls}
            renderItem={image => (
                <List.Item
                    extra={
                        <div style={image_with_cross}>
                            <Image
                                width={300}
                                src={image.image}
                            />

                            <Button
                                type="text"
                                icon={<CloseOutlined/>}
                                onClick={onRemove(image.id)}
                                size={'middle'}
                                danger
                            />
                        </div>}
                >
                    <List.Item.Meta
                        title={<text style={{color: "green"}}>Пневомнии нет</text>}
                        description={image.date}
                    />

                    {<ImageInfo image={image} afterUpdateImage={getImagesByPatient}/>}
                </List.Item>
            )}
        />
    )
}