import React, {useState} from 'react';
import 'antd/dist/antd.css';
import {Button, Input} from "antd";
import {IImage} from "../domains";
import {EditOutlined} from '@ant-design/icons';
import axios from "axios";

const {TextArea} = Input;

interface IProps {
    image: IImage;
    afterUpdateImage: () => void;
}

export const ImageInfo = ({image, afterUpdateImage}: IProps) => {
    const [isEdit, setEdit] = useState(false);
    const [description, setDescription] = useState(image.description);

    const onEdit = (isEdit: boolean) => () => {
        setEdit(isEdit);
    }

    const onChange = e => {
        const {value} = e.target;

        setDescription(value);
    }

    const onUpdate = (id) => () => {
        image.description = description

        axios.put(`images/${id}`, JSON.stringify(image), {
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
        }).then(() => setEdit(false))
            .then(() => afterUpdateImage())
    }

    return isEdit ? (
            <div>
                <TextArea rows={4} defaultValue={description} onChange={onChange}/>

                <Button
                    type="text"
                    onClick={onEdit(false)}
                    size={'middle'}
                >
                    Отмена
                </Button>

                <Button
                    type="text"
                    onClick={onUpdate(image.id)}
                    size={'middle'}
                >
                    Изменить
                </Button>
            </div>
        )
        : (
            <div>
                <span style={{fontWeight: "bold"}}>Замечания врача: </span>
                {description}

                <Button
                    type="text"
                    icon={<EditOutlined/>}
                    onClick={onEdit(true)}
                    size={'middle'}
                />
            </div>
        )
}