import React, {CSSProperties, useEffect, useState} from 'react';
import 'antd/dist/antd.css';
import {Breadcrumb, Card, Divider} from "antd";
import {ImageUploader} from "../ImageUploader";
import {IImage, IPatient} from "../domains";
import {DateTime, Interval} from "luxon";
import {useHistory, useLocation} from "react-router-dom";
import {ListImages} from "./ListImages";
import LogoutButton from "../LogoutButton";
import axios from "axios";

const flexStyle: CSSProperties = {
    display: "flex",
    justifyContent: "space-between",
    padding: 16,
}

export const PatientPage = () => {
    const history = useHistory();
     const location = useLocation();
    const [patient, setPatient] = useState({} as IPatient);
    const [urls, setUrls] = useState([] as IImage[]);

    useEffect(() => {
         // @ts-ignore
        const selectedPatient = location.state.selectedPatient;

        axios.get(`patients/${selectedPatient}`)
            .then(response => {
                return response.data;
            })
            .then(patient => {
                setPatient(patient);
            });

        getImagesByPatient();
    }, []);

    const getImagesByPatient = () => {
         // @ts-ignore
        const selectedPatient = location.state.selectedPatient;

        axios.get(`imagesByPatient/${selectedPatient}`)
            .then(response => {
                return response.data;
            })
            .then(urls => {
                urls.forEach(url => {
                    url.date = DateTime.fromISO(url.date).setLocale('ru').toFormat('dd MMMM yyyy')
                })
                setUrls(urls);
            })
    }

    const {id, last_name, middle_name, name, address, birthday, phone, is_pneumonia} = patient

    let startDate = DateTime.fromISO(birthday)
    let endDate = DateTime.local();

    let intv = Interval.fromDateTimes(startDate, endDate).length('years');

    return (
        <div>
            <Breadcrumb style={{padding: 16}}>
                <Breadcrumb.Item separator onClick={() => {
                    history.push("/");
                }}>
                    <a>Главная</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Пациент</Breadcrumb.Item>
            </Breadcrumb>

             <LogoutButton/>

            <Divider style={{margin: 0}}/>
            <Card title={`${last_name} ${name} ${middle_name}`} bordered={false}
                  style={{width: "80%", margin: "0 auto"}}>
                <div style={flexStyle}>
                    <div>
                        <p>Возраст: {Math.floor(intv)}</p>
                        <p>Адрес: {address}</p>
                        <p>Контактный телефон: {phone}</p>
                    </div>

                    <div style={{width: "60%"}}>
                        <ImageUploader patientId={id} afterUpLoadImage={getImagesByPatient}/>
                    </div>
                </div>

                <Divider/>

                <ListImages urls={urls} getImagesByPatient={getImagesByPatient}/>
            </Card>
        </div>
    );
};
