export interface IPatient {
    address: string,
    birthday: string,
    id: number,
    is_pneumonia: boolean,
    last_name: string,
    middle_name: string,
    name: string,
    phone: string,
}

export interface IImage {
    id: number,
    image: string,
    date: string,
    patient: number,
    description: string,
}

export interface IAction {
    type: string;
    payload?: any;
}

export interface IPatientFilter {
    last_name?: string;
    name?: string;
    middle_name?: string;
    birthday?: string;
}

export interface IUser {
    token: string;
    user: {
        username: string;
    }
}