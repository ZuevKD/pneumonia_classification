import {Button} from "antd";
import React from "react";
import {bindActionCreators, Dispatch} from "redux";
import {logout} from "../store/actionCreators";
import {connect} from "react-redux";

interface IDispatchProps {
    logout: () => void;
}

type IProps = IDispatchProps;

const LogoutButton = ({logout}: IProps) => {
    const onLogout = () => {
        logout();
    }

    return (
        <Button style={{
            right: 16, top: 10, zIndex: 2147483640, flexDirection: "column", position: "fixed",
        }}
                onClick={onLogout}
        >
            Выход
        </Button>
    )
}

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators({
        logout,
    }, dispatch);

export default connect<null, IDispatchProps>(
    null,
    mapDispatchToProps,
)(LogoutButton);