import React from 'react';
import {connect} from "react-redux";
import {ConnectedRouter} from "connected-react-router";
import {history} from "./store/reducers/configureStore";
import {Route, Switch} from "react-router";
import {LoginPage} from "./components/LoginPage/LoginPage";
import {PatientPage} from "./components/PatientPage/PatientPage";
import MainPage from "./components/MainPage/MainPage";

interface IStateProps {
    token: string;
}

type IProps = IStateProps;

const Router = ({token}: IProps) => {
    console.log(localStorage.getItem("token"))
    if (localStorage.getItem("token") == null) {
       return <LoginPage />
    }

    return (
        <ConnectedRouter history={history}>
            <Switch>
                <Route exact path="/" component={MainPage}/>
                <Route path="/patient" component={PatientPage}/>
            </Switch>
        </ConnectedRouter>
    )
}

const mapStateToProps = (state: any): IStateProps => {
    console.log(state.auth)
    return ({
        token: state.auth.user.token,
    });
};

export default connect<IStateProps>(
    mapStateToProps,
)(Router);