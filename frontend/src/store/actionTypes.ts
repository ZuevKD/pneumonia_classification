export const LOAD_PATIENTS = "LOAD_PATIENTS";
export const LOAD_PATIENT = "LOAD_PATIENT";
export const AUTH = "AUTH";
export const LOGOUT = "LOGOUT";