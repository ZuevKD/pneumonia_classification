import * as actionTypes from "./actionTypes"
import {Action} from "redux";
import axios from 'axios';
import {IPatientFilter} from "../components/domains";

axios.defaults.headers.common['Authorization'] = 'Token ' + localStorage.getItem('token');

export const loadPatients = (filter?: IPatientFilter): Action | any => {
    return (dispatch: any) => {
        return axios.get('/patients', {
            params: filter
        })
            .then(response => {
                dispatch({
                    type: actionTypes.LOAD_PATIENTS,
                    payload: {
                        response: response.data,
                    }
                })
            })
            .catch(error => {
                throw(error);
            });
    };
};

export const loadPatient = (id: number): Action | any => {
    return (dispatch: any) => {
        return axios.get(`/patients/${id}`)
            .then(response => {
                dispatch({
                    type: actionTypes.LOAD_PATIENT,
                    payload: {
                        response: response.data,
                    }
                })
            })
            .catch(error => {
                throw(error);
            });
    };
};

export const auth = (username: string, password: string): Action | any => {
    return (dispatch: any) => {
        return axios.post(`/token-auth/`, {username, password})
            .then(response => {
                dispatch({
                    type: actionTypes.AUTH,
                    payload: {
                        response: response.data,
                    }
                })
            })
            .catch(error => {
                throw(error);
            });
    };
};

export const logout = (): Action | any => {
    return (dispatch: any) => {
        dispatch({
            type: actionTypes.LOGOUT,
        })
    };
};