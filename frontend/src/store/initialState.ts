import {IPatient, IImage, IUser} from "../components/domains";

export interface IAppInfo {
    mainPage: {
        patients: IPatient[],
    },
    patientPage: {
        patient: IPatient,
        images: IImage[],
    },
    user: IUser,
}

export const initialState: IAppInfo = {
    mainPage: {
        patients: [],
    },
    patientPage: {
        patient: {} as IPatient,
        images: [],
    },
    user: {
        token: '',
        user: {
            username: '',
        }
    }
};