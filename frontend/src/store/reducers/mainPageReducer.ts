import {LOAD_PATIENTS} from "../actionTypes"
import {initialState} from "../initialState";
import {createReducer, updateObject} from "./utils";
import {IAction, IPatient} from "../../components/domains";
import {DateTime} from "luxon";

const loadPatients = (state: any, action: IAction) => {
    let patients = action.payload.response;
    patients.forEach((patient:IPatient) => {
                    patient.birthday = DateTime.fromISO(patient.birthday).setLocale('ru').toFormat('dd.MM.yyyy')
                })
    return updateObject(state, {patients})
}

const mainPageReducer = createReducer(initialState.mainPage, {
    [LOAD_PATIENTS]: loadPatients,
})

export default mainPageReducer