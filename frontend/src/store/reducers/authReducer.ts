import {AUTH, LOGOUT} from "../actionTypes"
import {initialState} from "../initialState";
import {createReducer, updateObject} from "./utils";
import {IAction} from "../../components/domains";

const auth = (state: any, action: IAction) => {
    const response = action.payload.response;
    localStorage.setItem('token', response.token)
    return updateObject(state, {user: response})
}

const logout = (state: any, action: IAction) => {
    localStorage.removeItem('token')

    return updateObject(state, {user: initialState.user})
}

const authReducer = createReducer(initialState.user, {
    [AUTH]: auth,
    [LOGOUT]: logout,
})

export default authReducer