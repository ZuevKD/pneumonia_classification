import {combineReducers} from "redux";
import mainPageReducer from "./mainPageReducer";
import patientPageReducer from "./patientPageReducer"
import {connectRouter} from 'connected-react-router'
import authReducer from "./authReducer";

const appReducer = (history: any) => combineReducers({
    auth: authReducer,
    mainPage: mainPageReducer,
    patientPage: patientPageReducer,
    router: connectRouter(history),
})

export default appReducer;