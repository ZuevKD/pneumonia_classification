import {createBrowserHistory} from 'history'
import {applyMiddleware, compose, createStore} from 'redux'
import {routerMiddleware} from 'connected-react-router'
import appReducer from "./appReducer";
import thunk from "redux-thunk";

export const history = createBrowserHistory()

export default function configureStore(preloadedState: any) {
    return createStore(
        appReducer(history),
        preloadedState,
        compose(
            applyMiddleware(
                thunk,
                routerMiddleware(history),
            ),
        ),
    )
}