import {IAction} from "../../components/domains";

export const updateObject = (oldObject: object, newValues: object) => ({
    ...oldObject,
    ...newValues,
})

export const createReducer = (initialState: object, handlers: any) => {
    return function reducer(state = initialState, action: IAction) {
        if (handlers.hasOwnProperty(action.type)) {
            return handlers[action.type](state, action)
        } else {
            return state
        }
    }
}