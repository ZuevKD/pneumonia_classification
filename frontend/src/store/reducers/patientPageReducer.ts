import {LOAD_PATIENT} from "../actionTypes"
import {initialState} from "../initialState";
import {createReducer, updateObject} from "./utils";
import {IAction} from "../../components/domains";

const loadPatient = (state: any, action: IAction) => {
    return updateObject(state, {patient: action.payload.response})
}

const patientPageReducer = createReducer(initialState.patientPage, {
    [LOAD_PATIENT]: loadPatient,
})

export default patientPageReducer