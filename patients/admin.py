from django.contrib import admin
from .models import Doctor, Diagnosis, Patient, Image

# Register your models here.
admin.site.register(Doctor)
admin.site.register(Diagnosis)
admin.site.register(Patient)
admin.site.register(Image)
