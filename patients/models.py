from django.db import models


class Doctor(models.Model):
    last_name = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    middle_name = models.CharField(max_length=200)
    username = models.CharField(max_length=200)
    password = models.CharField(max_length=200)


class Patient(models.Model):
    last_name = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    middle_name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone = models.CharField(max_length=200)
    is_pneumonia = models.BooleanField()
    birthday = models.DateField("date published")


class Diagnosis(models.Model):
    doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE)
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
    diagnosis_text = models.TextField()
    date = models.DateTimeField("date published")


class Image(models.Model):
    image = models.ImageField(upload_to='images')
    date = models.DateTimeField("date published")
    description = models.CharField(max_length=1000, default="-")
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
