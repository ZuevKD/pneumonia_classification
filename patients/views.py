from django.contrib.auth.models import User
from django.utils import timezone

from rest_framework import generics, permissions
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Doctor, Patient, Image
from .serializers import DoctorSerializer, LungsImageSerializer, PatientSerializer, UserSerializer, \
    UserSerializerWithToken


class DoctorListCreate(generics.ListCreateAPIView):
    queryset = Doctor.objects.all()
    serializer_class = DoctorSerializer


class PatientListCreate(generics.ListCreateAPIView):
    queryset = Patient.objects.all()
    serializer_class = PatientSerializer


@api_view(['GET', 'PUT', 'DELETE'])
def patient_detail(request, pk):
    try:
        patient = Patient.objects.get(pk=pk)
    except Patient.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = PatientSerializer(patient)
        return Response(serializer.data)


class PatientList(generics.ListAPIView):
    serializer_class = PatientSerializer

    def get_queryset(self):
        last_name = self.request.query_params.get('last_name')
        name = self.request.query_params.get('name')
        middle_name = self.request.query_params.get('middle_name')
        birthday = self.request.query_params.get('birthday')

        if birthday is None:
            if last_name is None:
                return Patient.objects.all()

            if name is None:
                return Patient.objects.extra(where=["last_name LIKE CONCAT('%%',%s,'%%')"], params=[last_name])

            if middle_name is None:
                return Patient.objects.extra(where=["last_name LIKE CONCAT('%%',%s,'%%') "
                                                    "and name LIKE CONCAT('%%',%s,'%%')"], params=[last_name, name])

            return Patient.objects.extra(where=["last_name LIKE CONCAT('%%',%s,'%%') "
                                                "and name LIKE CONCAT('%%',%s,'%%') "
                                                "and middle_name LIKE CONCAT('%%',%s,'%%')"],
                                         params=[last_name, name, middle_name])

        else:
            if last_name is None:
                return Patient.objects.extra(where=["last_name LIKE CONCAT('%%',%s,'%%')"
                                                    "and birthday = %s"], params=[last_name, birthday])

            if name is None:
                return Patient.objects.extra(where=["last_name LIKE CONCAT('%%',%s,'%%')"
                                                    "and birthday = %s"], params=[last_name, birthday])

            if middle_name is None:
                return Patient.objects.extra(where=["last_name LIKE CONCAT('%%',%s,'%%') "
                                                    "and name LIKE CONCAT('%%',%s,'%%')"
                                                    "and birthday = %s"], params=[last_name, name, birthday])

            return Patient.objects.extra(where=["last_name LIKE CONCAT('%%',%s,'%%') "
                                                "and name LIKE CONCAT('%%',%s,'%%') "
                                                "and middle_name LIKE CONCAT('%%',%s,'%%')"
                                                "and birthday = %s"],
                                         params=[last_name, name, middle_name, birthday])


class ImagesByPatientView(generics.ListAPIView):
    serializer_class = LungsImageSerializer

    def get_queryset(self):
        patient_id = self.kwargs['patient_id']
        return Image.objects.filter(patient=patient_id).order_by('date')


class ImageView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Image.objects.all()
    serializer_class = LungsImageSerializer

    def put(self, request, *args, **kwargs):
        pk = kwargs['pk']
        image = Image.objects.get(pk=pk)

        request.data.__setitem__("image", image.image)
        request.data.__setitem__("date", image.date)

        serializer = LungsImageSerializer(data=request.data)

        if serializer.is_valid():
            request.data.__setitem__("patient", image.patient)
            serializer.update(image, validated_data=request.data)
            return Response(serializer.data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LungsImageUpload(APIView):
    parser_classes = [MultiPartParser, FormParser]
    queryset = User.objects.none()

    permission_classes = [permissions.DjangoModelPermissions]
    def post(self, request):
        request.data.__setitem__("patient", request.data.__getitem__("patientId"))
        request.data.__setitem__("date", timezone.now())

        serializer = LungsImageSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def current_user(request):
    """
    Determine the current user by their token, and return their data
    """

    serializer = UserSerializer(request.user)
    return Response(serializer.data)


class UserList(APIView):
    """
    Create a new user. It's called 'UserList' because normally we'd have a get
    method here too, for retrieving a list of all User objects.
    """

    permission_classes = (permissions.AllowAny,)

    def post(self, request):
        serializer = UserSerializerWithToken(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
