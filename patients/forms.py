from django import forms

from patients.models import Image


class ImageForm(forms.ModelForm):
    class Meta:
        model = Image
        fields = ['name', 'image']
