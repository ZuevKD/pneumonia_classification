from django.urls import path

from . import views
from rest_framework_jwt.views import obtain_jwt_token

from .views import current_user, UserList

urlpatterns = [
    path('current_user/', current_user),
    path('users/', UserList.as_view()),
    path('token-auth/', obtain_jwt_token),
    path('doctors/', views.DoctorListCreate.as_view()),
    path('patients/', views.PatientList.as_view()),
    path('patients/<int:pk>', views.patient_detail),
    path('imagesByPatient/<int:patient_id>', views.ImagesByPatientView.as_view()),
    path('images/', views.ImageView.as_view()),
    path('images/<int:pk>', views.ImageView.as_view()),
    path('upload-image', views.LungsImageUpload.as_view()),
]
