# Система для распознавания пневмонии по рентгеновским снимкам

Для запуска нужно выбрать интерпретатор python

Запускаем файл manage.py с помощью клавиш Ctrl + Alt + R
Если не работает, то заходим в Settings/Preferences (Ctrl+Alt+S) Выбирает пункт Languages and Frameworks, 
и выбираем Django.

Затем вводим команду `runserver`
 
Для запуска БД нужно загрузить psycopg2
`pip install psycopg2`
- делаем миграцию ./manage.py migrate,
- создаем суперпользователя ./manage.py createsuperuser
- и запускаем сервер ./manage runserver.

Затем нужно ввести `makemigrations`, `sqlmigrate patients 0001` и `migrate` для создания таблиц

Petrov-VV
test_user123

Titov-AA
test_user123